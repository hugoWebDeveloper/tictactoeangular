import { Component } from '@angular/core';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() { }

  title = 'juegoTicTaeToe';

  f1: boolean = false;
  f2: boolean = false;
  f3: boolean = false;
  f4: boolean = false;
  f5: boolean = false;
  f6: boolean = false;
  f7: boolean = false;
  f8: boolean = false;
  f9: boolean = false;
  contador: number = 1;
  hayGanador: boolean = false;
  iniciar: boolean = false;
  mensajeGanador: string;


  jugador1: boolean = true;//0
  jugador2: boolean = false;//x



  marcarCuadro(id: number) {
    if (this.contador == 9 && this.hayGanador == false) {
      alert("no hubo ganador")
    } else {

      if (this.hayGanador == false) {

        if (document.getElementById("icon" + id + "").innerHTML == "") {

          if (this.iniciar == true) {

            this.contador++;

            if (this.jugador1 == true) {
              const element = document.getElementById("icon" + id + "");
              element.innerHTML = "O";
              this.jugador1 = false;
              this.jugador2 = true;
            } else {
              const element = document.getElementById("icon" + id + "");
              element.innerHTML = "X";
              this.jugador1 = true;
              this.jugador2 = false;
            }

            if ((this.f1 && this.f2 && this.f3) &&
              ((document.getElementById('icon1').innerHTML == "X"
                && document.getElementById('icon2').innerHTML == "X"
                && document.getElementById('icon3').innerHTML == "X")
                || (document.getElementById('icon1').innerHTML == "O"
                  && document.getElementById('icon2').innerHTML == "O"
                  && document.getElementById('icon3').innerHTML == "O"
                ))
            ) {
              this.hayGanador = true;
              if (this.jugador1 == true) {
                this.mensajeGanador = "Ganó el jugador 2";
              } else {
                this.mensajeGanador = "Ganó el jugador 1";
              }

            }
            if ((this.f4 && this.f5 && this.f6) &&
              ((document.getElementById('icon4').innerHTML == "X"
                && document.getElementById('icon5').innerHTML == "X"
                && document.getElementById('icon6').innerHTML == "X")
                || (document.getElementById('icon4').innerHTML == "O"
                  && document.getElementById('icon5').innerHTML == "O"
                  && document.getElementById('icon6').innerHTML == "O"
                ))
            ) {
              this.hayGanador = true;
              if (this.jugador1 == true) {
                this.mensajeGanador = "Ganó el jugador 2";
              } else {
                this.mensajeGanador = "Ganó el jugador 1";
              }
            }
            if ((this.f7 && this.f8 && this.f9) &&
              ((document.getElementById('icon7').innerHTML == "X"
                && document.getElementById('icon8').innerHTML == "X"
                && document.getElementById('icon9').innerHTML == "X")
                || (document.getElementById('icon7').innerHTML == "O"
                  && document.getElementById('icon8').innerHTML == "O"
                  && document.getElementById('icon9').innerHTML == "O"
                ))
            ) {
              this.hayGanador = true;
              if (this.jugador1 == true) {
                this.mensajeGanador = "Ganó el jugador 2";
              } else {
                this.mensajeGanador = "Ganó el jugador 1";
              }
            }
            if ((this.f1 && this.f4 && this.f7) &&
              ((document.getElementById('icon1').innerHTML == "X"
                && document.getElementById('icon4').innerHTML == "X"
                && document.getElementById('icon7').innerHTML == "X")
                || (document.getElementById('icon1').innerHTML == "O"
                  && document.getElementById('icon4').innerHTML == "O"
                  && document.getElementById('icon7').innerHTML == "O"
                ))
            ) {
              this.hayGanador = true;
              if (this.jugador1 == true) {
                this.mensajeGanador = "Ganó el jugador 2";
              } else {
                this.mensajeGanador = "Ganó el jugador 1";
              }
            }
            if ((this.f2 && this.f5 && this.f8) &&
              ((document.getElementById('icon2').innerHTML == "X"
                && document.getElementById('icon5').innerHTML == "X"
                && document.getElementById('icon8').innerHTML == "X")
                || (document.getElementById('icon2').innerHTML == "O"
                  && document.getElementById('icon5').innerHTML == "O"
                  && document.getElementById('icon8').innerHTML == "O"
                ))
            ) {
              this.hayGanador = true;
              if (this.jugador1 == true) {
                this.mensajeGanador = "Ganó el jugador 2";
              } else {
                this.mensajeGanador = "Ganó el jugador 1";
              }
            }
            if ((this.f3 && this.f6 && this.f9) &&
              ((document.getElementById('icon3').innerHTML == "X"
                && document.getElementById('icon6').innerHTML == "X"
                && document.getElementById('icon9').innerHTML == "X")
                || (document.getElementById('icon3').innerHTML == "O"
                  && document.getElementById('icon6').innerHTML == "O"
                  && document.getElementById('icon9').innerHTML == "O"
                ))
            ) {
              this.hayGanador = true;
              if (this.jugador1 == true) {
                this.mensajeGanador = "Ganó el jugador 2";
              } else {
                this.mensajeGanador = "Ganó el jugador 1";
              }
            }
            if ((this.f1 && this.f5 && this.f9) &&
              ((document.getElementById('icon1').innerHTML == "X"
                && document.getElementById('icon5').innerHTML == "X"
                && document.getElementById('icon9').innerHTML == "X")
                || (document.getElementById('icon1').innerHTML == "O"
                  && document.getElementById('icon5').innerHTML == "O"
                  && document.getElementById('icon9').innerHTML == "O"
                ))
            ) {
              this.hayGanador = true;
              if (this.jugador1 == true) {
                this.mensajeGanador = "Ganó el jugador 2";
              } else {
                this.mensajeGanador = "Ganó el jugador 1";
              }
            }
            if ((this.f3 && this.f5 && this.f7) &&
              ((document.getElementById('icon3').innerHTML == "X"
                && document.getElementById('icon5').innerHTML == "X"
                && document.getElementById('icon7').innerHTML == "X")
                || (document.getElementById('icon3').innerHTML == "O"
                  && document.getElementById('icon5').innerHTML == "O"
                  && document.getElementById('icon7').innerHTML == "O"
                ))
            ) {
              this.hayGanador = true;
              if (this.jugador1 == true) {
                this.mensajeGanador = "Ganó el jugador 2";
              } else {
                this.mensajeGanador = "Ganó el jugador 1";
              }
            }

          } else {
            alert("Porfavor inicie la partida con el botón 'Iniciar Partida'");
          }
        } else {
          alert("Elija otra casilla");
        }
      } else {
        alert("Ya hay ganador, inicie otra partida");
      }
    }

  }

  iniciarPartida() {
    this.terminarPartida();
    this.iniciar = true;
  }

  terminarPartida() {
    this.f1 = false;
    this.f2 = false;
    this.f3 = false;
    this.f4 = false;
    this.f5 = false;
    this.f6 = false;
    this.f7 = false;
    this.f8 = false;
    this.f9 = false;
    this.contador = 0;
    this.hayGanador = false;
    this.iniciar = false;


    this.jugador1 = true;//0
    this.jugador2 = false;//x

    document.getElementById("icon1").innerHTML = "";
    document.getElementById("icon2").innerHTML = "";
    document.getElementById("icon3").innerHTML = "";
    document.getElementById("icon4").innerHTML = "";
    document.getElementById("icon5").innerHTML = "";
    document.getElementById("icon6").innerHTML = "";
    document.getElementById("icon7").innerHTML = "";
    document.getElementById("icon8").innerHTML = "";
    document.getElementById("icon9").innerHTML = "";

  }
}
